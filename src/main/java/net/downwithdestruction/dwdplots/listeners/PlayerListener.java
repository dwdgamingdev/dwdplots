package net.downwithdestruction.dwdplots.listeners;

import net.downwithdestruction.dwdplots.storage.Permit;
import net.downwithdestruction.dwdplots.storage.Plot;
import net.downwithdestruction.dwdplots.storage.State;
import net.downwithdestruction.dwdplots.storage.States;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerListener implements Listener {

	public PlayerListener() {
		
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.isCancelled()) {
			return;
		}
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.getItem() != null && event.getItem().getType() == Material.FEATHER) {
				// Show Plot information
				Location loc = event.getClickedBlock().getLocation();
				
				if(States.getPlotFromLocation(loc) != null) {
					Plot plot = States.getPlotFromLocation(loc);
					plot.printInfo(event.getPlayer());
				}
				else if(States.getPermitFromLocation(loc) != null) {
					Permit permit = States.getPermitFromLocation(loc);
					permit.printInfo(event.getPlayer());
					
				}
				else if(States.getStateFromLocation(loc) != null) {
					State state = States.getStateFromLocation(loc);
					state.printInfo(event.getPlayer());
				}
			}
		}
	}
}
