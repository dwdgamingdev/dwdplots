package net.downwithdestruction.dwdplots;

public class Coords {

	int x;
	int z;
	
	public Coords(int x, int z) {
		this.x = x;
		this.z = z;
	}
	
	public int getX() {
		return x;
	}
	
	public int getZ() {
		return z;
	}
}
