package net.downwithdestruction.dwdplots;

import java.util.HashMap;
import java.util.Map;


public class Util {
	
	private static Map<String,PlayerData> pd = new HashMap<String,PlayerData>();
	
	// Credit to StackExchange ^_^
	public static Coords getSpiralCoords(int n) {
	    if (n == 0) { 
	        return new Coords(0, 0);
	    }
	    // r = ring number.
	    int r = (int)(Math.ceil(0.5 * (-1.0 + Math.sqrt(1.0 + 2.0 * n))));
	    // n is the k-th number in ring r.
	    int k = n - 2 * (r - 1) * r - 1;
	    // n is the j-th number on its side of the ring. 
	    int j = k % r;
	    if (k < r) {
	        return new Coords(-j, r - j);
	    } else if (k < 2 * r) {
	        return new Coords(-r - j, -j);
	    } else if (k < 3 * r) {
	        return new Coords(j, -r - j);
	    } else {
	        return new Coords(r - j, j);
	    }
	}
	
	public static void addPlayerData(PlayerData dat) {
		pd.put(dat.getName(), dat);
	}
	
	public static PlayerData getPlayerData(String player) {
		if(pd.containsKey(player)) {
			return pd.get(player);
		}
		return null;
	}

	public static void removePlayerData(String name) {
		if(pd.containsKey(name)) {
			pd.remove(name);
		}
	}
	
}
