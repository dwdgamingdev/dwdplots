package net.downwithdestruction.dwdplots;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.downwithdestruction.core.lang.Lang;
import net.downwithdestruction.dwdplots.commands.GeneralCommands;
import net.downwithdestruction.dwdplots.listeners.PlayerListener;
import net.downwithdestruction.dwdplots.storage.States;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissionsException;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.CommandsManager;
import com.sk89q.minecraft.util.commands.MissingNestedCommandException;
import com.sk89q.minecraft.util.commands.WrappedCommandException;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class DwDPlotsPlugin extends JavaPlugin {
	
	private CommandsManager<CommandSender> commands;
	
	private static Lang lang;
	public static DwDPlotsPlugin plugin = null;
	public static Logger logger;
	public static DwDPlotsPlugin instance;
	
	public static String pluginName;
	public static String pluginVersion;
	public static Permission permission = null;
	public static Economy economy = null;

	public void onEnable() {
		
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		
		DwDPlotsPlugin.instance = this;
		DwDPlotsPlugin.logger = getLogger();
		DwDPlotsPlugin.pluginName = getDescription().getName();
		DwDPlotsPlugin.pluginVersion = getDescription().getVersion();
		
		DwDPlotsPlugin.lang = new Lang(this);
		PluginManager pm = getServer().getPluginManager();
		
		if (pm.getPlugin("Vault") != null) {
			setupEconomy();
		} else {
			log(ChatColor.RED
					+ "Missing dependency: Vault. Please install this for the plugin to work.");
			pm.disablePlugin(plugin);
			return;
		}
		
		if (pm.getPlugin("WorldGuard") != null) {
			setupEconomy();
		} else {
			log(ChatColor.RED
					+ "Missing dependency: WorldGuard. Please install this for the plugin to work.");
			pm.disablePlugin(plugin);
			return;
		}
		
		if (pm.getPlugin("WorldEdit") != null) {
			setupEconomy();
		} else {
			log(ChatColor.RED
					+ "Missing dependency: WorldEdit. Please install this for the plugin to work.");
			pm.disablePlugin(plugin);
			return;
		}
		
		// Create shops folder
		File theDir = new File(this.getDataFolder() + "/states/"
				+ File.separatorChar);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			boolean result = theDir.mkdir();
			if (result) {
				log("States folder created.");
			}
		}
		
		setupCommands();
		
		
		pm.registerEvents(new PlayerListener(), this);
		
		States.loadStates();
		
		log("Plugin enabled. - By TruDan");
		
	}
	
	public void onDisable() {
		
		States.saveStates();
		
		log("Plugin disabled. - By TruDan");
	}
	
	public static Lang getLang() {
		return lang;
	}

	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer()
				.getServicesManager().getRegistration(
						net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		try {
			this.commands.execute(cmd.getName(), args, sender, sender);
		} catch (CommandPermissionsException e) {
			sender.sendMessage(ChatColor.RED
					+ lang.get("exceptions.noPermission"));
		} catch (MissingNestedCommandException e) {
			sender.sendMessage(ChatColor.RED + e.getUsage());
		} catch (CommandUsageException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
			sender.sendMessage(ChatColor.RED + e.getUsage());
		} catch (WrappedCommandException e) {
			if (e.getCause() instanceof NumberFormatException) {
				sender.sendMessage(ChatColor.RED
						+ lang.get("exceptions.numExpected"));
			} else {
				sender.sendMessage(ChatColor.RED
						+ lang.get("exceptions.errorOccurred"));
				e.printStackTrace();
			}
		} catch (CommandException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
		}

		return true;
	}
	
	public WorldGuardPlugin getWorldGuard() {
	    Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) plugin;
	}
	
	public WorldEditPlugin getWorldEdit() {
	    Plugin plugin = getServer().getPluginManager().getPlugin("WorldEdit");
	 
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldEditPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldEditPlugin) plugin;
	}
	
	private void setupCommands() {
		this.commands = new CommandsManager<CommandSender>() {
			@Override
			public boolean hasPermission(CommandSender sender, String perm) {
				return sender.hasPermission(perm);
			}
		};

		CommandsManagerRegistration cmdRegister = new CommandsManagerRegistration(
				this, this.commands);
		cmdRegister.register(GeneralCommands.class);
	}
	
	public static void log(String message,Level level) {
		if(instance.getConfig().getBoolean("useFancyConsole") == true) {
			ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
			console.sendMessage("[" + ChatColor.DARK_RED + pluginName + " v" + pluginVersion + ChatColor.GRAY + "] " +  message);
		}
		else {
			DwDPlotsPlugin.logger.log(level,"["+pluginName+" v"+pluginVersion+"] "+message);
		}
	}
	
	public static void log(String message) {
		log(message,Level.INFO);
	}
	
	public static void debug(String message) {
		if(instance.getConfig().getBoolean("debug")) {
			log(message,Level.INFO);
		}
	}
	
	
}
