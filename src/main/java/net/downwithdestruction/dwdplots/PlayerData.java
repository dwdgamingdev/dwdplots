package net.downwithdestruction.dwdplots;

import java.util.Calendar;
import java.util.TimeZone;

import org.bukkit.entity.Player;

import net.downwithdestruction.dwdplots.storage.Permit;

public class PlayerData {

	private String name;
	
	private boolean plotConfirmationMode = false;
	private int plotConfirmationNumber = 0;
	private long plotConfirmationTimeout = 0L;
	private Permit permit;
	private Player sender;
	
	public PlayerData(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPermit(Permit permit) {
		this.permit = permit;
	}
	
	public void setSender(Player player) {
		this.sender = player;
	}
	
	public Player getSender() {
		return sender;
	}
	
	public void setPlotConfirmMode(int number) {
		this.plotConfirmationMode = true;
		this.plotConfirmationNumber = number;
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		long secondsSinceEpoch = calendar.getTimeInMillis() / 1000L;
		
		this.plotConfirmationTimeout = secondsSinceEpoch + 60;
	}
	
	public boolean checkConfirm() {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		long time = calendar.getTimeInMillis() / 1000L;
		if(plotConfirmationMode == true && plotConfirmationTimeout >= time) {
			return true;
		}
		plotConfirmationMode = false;
		plotConfirmationTimeout = 0L;
		plotConfirmationNumber = 0;
		return false;
	}
	
	public int getConfirmationNumber() {
		return plotConfirmationNumber;
	}
	
	public Permit getPermit() {
		return permit;
	}
}
