package net.downwithdestruction.dwdplots.storage;

import java.util.HashSet;
import java.util.Set;

public class PlotMember {

	private String name;
	private Set<PlotFlag> flags = new HashSet<PlotFlag>();
	
	public PlotMember(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void removeFlag(PlotFlag flag) {
		if(flags.contains(flag)) {
			flags.remove(flag);
		}
	}
	
	public void addFlag(PlotFlag flag) {
		if(!flags.contains(flag)) {
			flags.add(flag);
		}
	}
	
	public boolean hasFlag(PlotFlag flag) {
		if(flags.contains(flag)) {
			return true;
		}
		return false;
	}
	
	public PlotFlag[] getFlags() {
		return (PlotFlag[]) flags.toArray();
	}
}
