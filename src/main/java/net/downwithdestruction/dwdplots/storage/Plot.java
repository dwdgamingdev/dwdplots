package net.downwithdestruction.dwdplots.storage;

import java.util.HashMap;
import java.util.Map;

import net.downwithdestruction.dwdplots.DwDPlotsPlugin;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Plot {

	private String superOwner = "";
	private Permit permit;
	private int plotNo;
	private boolean forSale = true;
	
	int minX = 0;
	int maxX = 0;
	int minY = 0;
	int maxY = 0;
	int minZ = 0;
	int maxZ = 0;
	
	Location signLocation;
	
	private double price = 0.00;
	
	private Map<String, PlotMember> members = new HashMap<String, PlotMember>();
	
	public Plot(Permit permit, int plotNo) {
		this.plotNo = plotNo;
		this.permit = permit;
	}
	
	public String getSuperOwner() {
		return superOwner;
	}
	
	public int getPlotNo() {
		return plotNo;
	}
	
	public double getPrice() {
		return price;
	}
	
	public Permit getPermit() {
		return permit;
	}
	
	public boolean setPrice(double price) {
		int w = ((maxX > minX) ? maxX - minX : minX - maxX)+1;
		int l = ((maxZ > minZ) ? maxZ - minZ : minZ - maxZ)+1;
		int h = ((maxY > minY) ? maxY - minY : minY - maxY)+1;
		double guidePrice = (w*l*h) / 4;
		if(price >= guidePrice) {
			this.price = price;
			return true;
		}
		else {
			this.price = guidePrice;
			return false;
		}
	}
	
	public void setRegion(int x1, int y1, int z1, int x2, int y2, int z2) {
		this.minX = (x1 > x2) ? x2 : x1;
		this.minY = (y1 > y2) ? y2 : y1;
		this.minZ = (z1 > z2) ? z2 : z1;
		this.maxX = (x1 < x2) ? x2 : x1;
		this.maxY = (y1 < y2) ? y2 : y1;
		this.maxZ = (z1 < z2) ? z2 : z1;
	}
	
	public PlotMember[] getMembers() {
		return (PlotMember[]) members.values().toArray( new PlotMember[0]);
	}
	
	public boolean isMember(String name) {
		for(PlotMember member : members.values()) {
			if(member.getName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}
	
	public PlotMember getMember(String name) {
		for(PlotMember member : members.values()) {
			if(member.getName().equalsIgnoreCase(name)) {
				return member;
			}
		}
		return null;
	}
	
	public void removeMember(String name) {
		for(PlotMember member : members.values()) {
			if(member.getName().equalsIgnoreCase(name)) {
				members.remove(member);
				return;
			}
		}
	}
	
	public boolean memberHas(String name, PlotFlag flag) {
		if(isSuperOwner(name)) {
			return true;
		}
		for(PlotMember member : members.values()) {
			if(member.getName().equalsIgnoreCase(name)) {
				return member.hasFlag(flag);
			}
		}
		return false;
	}
	
	public boolean containsLocation(Location loc) {

		if (minX <= loc.getBlockX() && loc.getBlockX() <= maxX
				&& minY <= loc.getBlockY() && loc.getBlockY() <= maxY
				&& minZ <= loc.getBlockZ() && loc.getBlockZ() <= maxZ) {
			return true;
		}
		return false;
	}
	
	public ProtectedRegion getRegion() {
		WorldGuardPlugin wg = DwDPlotsPlugin.instance.getWorldGuard();
		RegionManager rm = wg.getRegionManager(Bukkit.getWorld(this.getPermit().getState().getName()));
		ProtectedRegion plotRegion = rm.getRegion(this.getPermit().getName().toLowerCase()+"_plot"+getPlotNo());
		return plotRegion;
	}
	
	public void setSuperOwner(String superOwner) {
		ProtectedRegion plotRegion = getRegion();
		
		DefaultDomain members = plotRegion.getMembers();
		if(getSuperOwner() != null) {
			members.removePlayer(getSuperOwner());
		}
		members.addPlayer(superOwner);
		
		this.superOwner = superOwner;
	}
	
	public void setForSale(boolean forSale) {
		this.forSale = forSale;
	}

	public void addMember(PlotMember pm) {
		if(!members.containsKey(pm.getName())) {
			members.put(pm.getName(),pm);
		}
	}

	public boolean isForSale() {
		return forSale;
	}

	public int getMinX() {
		return minX;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMinY() {
		return minY;
	}

	public int getMaxY() {
		return maxY;
	}

	public int getMinZ() {
		return minZ;
	}

	public int getMaxZ() {
		return maxZ;
	}

	public boolean isSuperOwner(String playername) {
		if(superOwner.equalsIgnoreCase(playername)) {
			return true;
		}
		return false;
	}
	
	public void printInfo(Player player) {

		int w = ((maxX > minX) ? maxX - minX : minX - maxX)+1;
		int l = ((maxZ > minZ) ? maxZ - minZ : minZ - maxZ)+1;
		int h = ((maxY > minY) ? maxY - minY : minY - maxY)+1;
		
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.plot.info.header").replaceAll("%I", ""+getPlotNo()).replaceAll("%P", getPermit().getName()));
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.plot.info.owner").replaceAll("%O", getSuperOwner()));
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.plot.info.size").replaceAll("%S", w+"x"+l+"x"+h));
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.plot.info.price").replaceAll("%P", ""+getPrice()));
		
		int x = 0;
		String members = "";
		for(PlotMember member : getMembers()) {
			if(x > 0) members += ", ";
			members += member.getName();
			x++;
		}
		
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.plot.info.members").replaceAll("%M", members));
		return;
	}
	
	public void locateSign() {
		for(int a=0;a<3;a++) {
			for(int x=(minX-a);x<=(maxX+a);x++) {
				for(int y=(minY-a);y<=(maxY+a);y++) {
					for(int z=(minZ-a);z<=(maxZ+a);z++) {
						if(Bukkit.getWorld(getPermit().getState().getName()) != null) {
							Block block = Bukkit.getWorld(getPermit().getState().getName()).getBlockAt(x, y, z);
							if(block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN || block.getType() == Material.SIGN) {
								DwDPlotsPlugin.debug("Found a sign at: "+x+", "+y+", "+z);
								Sign sign = (Sign) block.getState();
								if(sign.getLine(1).equalsIgnoreCase("[plot]")) {
									this.signLocation = sign.getLocation();
									return;
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void updateSign() {
		
	}
	
}
