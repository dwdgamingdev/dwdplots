package net.downwithdestruction.dwdplots.storage;

import java.util.HashSet;
import java.util.Set;

public class PermitMember {

	private String name;
	private Set<PermitFlag> flags = new HashSet<PermitFlag>();
	
	public PermitMember(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void removeFlag(PermitFlag flag) {
		if(flags.contains(flag)) {
			flags.remove(flag);
		}
	}
	
	public void addFlag(PermitFlag flag) {
		if(!flags.contains(flag)) {
			flags.add(flag);
		}
	}
	
	public boolean hasFlag(PermitFlag flag) {
		if(flags.contains(flag)) {
			return true;
		}
		return false;
	}
	
	public PermitFlag[] getFlags() {
		return (PermitFlag[]) flags.toArray(new PermitFlag[0]);
	}
}
