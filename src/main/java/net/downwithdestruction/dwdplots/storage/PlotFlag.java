package net.downwithdestruction.dwdplots.storage;

public enum PlotFlag {

	BUILD("BUILD","Build Access","Allows the user to build inside the plot."),
	MEMBERS_ADD("MEMBERS_ADD","Add Members","Allows the user to add members to the plot."),
	MEMBERS_MODIFY("MEMBERS_MODIFY", "Modify Members", "Allows the user to modify existing members of the plot."),
	MEMBERS_REMOVE("MEMBERS_REMOVE", "Remove Members", "Allows the user to remove existing members of the plot."),
	;
	
	private String name,help,ufName;
	
	PlotFlag(String name, String ufName, String help) {
		this.name = name;
		this.help = help;
		this.ufName = ufName;
	}
	
	public String getName() {
		return name;
	}
	
	public String getHelp() {
		return help;
	}

	public static PlotFlag getEnumByName(String name) {
		for(PlotFlag flag : PlotFlag.values()) {
			if(flag.getName().equals(name)) {
				return flag;
			}
		}
		return null;
	}

	public String getUfName() {
		return ufName;
	}
}
