package net.downwithdestruction.dwdplots.storage;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion.CircularInheritanceException;

import net.downwithdestruction.dwdplots.Coords;
import net.downwithdestruction.dwdplots.DwDPlotsPlugin;
import net.downwithdestruction.dwdplots.Util;

public class Permit {

	int permitNo = 0;
	int minX = 0;
	int maxX = 0;
	int minY = 0;
	int maxY = 0;
	int minZ = 0;
	int maxZ = 0;
	int gridMinX = 0;
	int gridMaxX = 0;
	int gridMinY = 0;
	int gridMaxY = 0;
	int gridMinZ = 0;
	int gridMaxZ = 0;
	int radius = 0;
	String superOwner = "";
	String name = "";
	
	String plotSign1 = "";
	String plotSign2 = "";
	String plotSign3 = "";
	String plotSign4 = "";

	State state;

	private Map<String, PermitMember> members = new HashMap<String, PermitMember>();
	private Map<Integer, Plot> plots = new HashMap<Integer, Plot>();

	public Permit(State state, int permitNo, String name) {
		this.name = name;
		Coords coords = Util.getSpiralCoords(permitNo);
		int maxRadius = 3500;

		this.gridMinX = ((coords.getX()) * maxRadius) - (maxRadius / 2);
		this.gridMinZ = ((coords.getZ()) * maxRadius) - (maxRadius / 2);

		this.gridMaxX = gridMinX + maxRadius + 1;
		this.gridMaxZ = gridMinZ + maxRadius + 1;

		this.gridMinY = 0;
		this.gridMaxY = 256;

		this.minY = 0;
		this.maxY = 256;

		int halfSize = (DwDPlotsPlugin.instance.getConfig()
				.getInt("defaultPermitSize")) / 2;
		this.radius = halfSize;

		this.minX = ((maxRadius / 2) - halfSize) + gridMinX;
		this.maxX = ((maxRadius / 2) + 1 + halfSize) + gridMinX;

		this.minZ = ((maxRadius / 2) - halfSize) + gridMinZ;
		this.maxZ = ((maxRadius / 2) + 1 + halfSize) + gridMinZ;

		this.permitNo = permitNo;
		this.state = state;

	}

	public Permit(State state, int permitNo, String name, String superOwner,
			int minX, int maxX, int minY, int maxY, int minZ, int maxZ,
			int radius, int gridMinX, int gridMaxX, int gridMinY, int gridMaxY,
			int gridMinZ, int gridMaxZ) {
		this.permitNo = permitNo;
		this.name = name;
		this.superOwner = superOwner;
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
		this.minZ = minZ;
		this.maxZ = maxZ;
		this.radius = radius;
		this.gridMinX = gridMinX;
		this.gridMaxX = gridMaxX;
		this.gridMinY = gridMinY;
		this.gridMaxY = gridMaxY;
		this.gridMinZ = gridMinZ;
		this.gridMaxZ = gridMaxZ;
		this.state = state;
	}

	public State getState() {
		return state;
	}

	public int getNextPlotNumber() {
		int x = 1;
		while (x < DwDPlotsPlugin.instance.getConfig().getInt(
				"maximumAllowedPlotsPerPermit", 1000)) {
			if (!plots.containsKey(x)) {
				return x;
			}
			x++;
		}
		return x;
	}

	public Plot createPlot() {
		int plotNo = getNextPlotNumber();
		Plot plot = new Plot(this, plotNo);
		plots.put(plotNo, plot);
		return plot;
	}

	public Plot createPlot(int number) {
		int plotNo = number;
		Plot plot = new Plot(this, plotNo);
		plots.put(plotNo, plot);
		return plot;
	}

	public void deletePlot(int number) {
		if (plots.containsKey(number)) {
			plots.remove(number);
		}
	}

	public boolean canExpand(int amount) {
		if (amount + radius > 3000)
			return true;
		return false;
	}

	public boolean containsLocation(Location loc) {

		if (minX <= loc.getBlockX() && loc.getBlockX() <= maxX
				&& minY <= loc.getBlockY() && loc.getBlockY() <= maxY
				&& minZ <= loc.getBlockZ() && loc.getBlockZ() <= maxZ) {
			return true;
		}
		return false;
	}

	public Plot getPlotFromLocation(Location loc) {
		if (plots.size() > 0) {
			for (Plot plot : plots.values()) {
				if (plot.containsLocation(loc)) {
					return plot;
				}
			}
		}
		return null;
	}

	public PermitMember getMember(String player) {
		if (members.containsKey(player)) {
			return members.get(player);
		}
		return null;
	}

	public boolean isMember(String player) {
		if (members.containsKey(player)) {
			return true;
		}
		return false;
	}

	public boolean isSuperOwner(String player) {
		if (superOwner.equals(player)) {
			return true;
		}
		return false;
	}

	public boolean memberHas(String player, PermitFlag flag) {
		if (isSuperOwner(player)) {
			return true;
		}
		if (isMember(player)) {
			return getMember(player).hasFlag(flag);
		}
		return false;
	}

	public void addMember(PermitMember member) {
		if (!members.containsKey(member.getName())) {
			members.put(member.getName(), member);
		}
	}

	public void removeMember(String membername) {
		if (members.containsKey(membername)) {
			members.remove(membername);
		}
	}

	public Collection<PermitMember> getMembers() {
		return members.values();
	}

	public Collection<Plot> getPlots() {
		return plots.values();
	}

	public Plot getPlot(int plotID) {
		if (plots.containsKey(plotID)) {
			return plots.get(plotID);
		}
		return null;
	}

	public Coords getCoords() {
		return Util.getSpiralCoords(permitNo);
	}

	public String getName() {
		return name;
	}

	public int getPermitNo() {
		DwDPlotsPlugin.debug("Permit No:" + permitNo);
		return permitNo;
	}

	public int getMinX() {
		return minX;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMinY() {
		return minY;
	}

	public int getMaxY() {
		return maxY;
	}

	public int getMinZ() {
		return minZ;
	}

	public int getMaxZ() {
		return maxZ;
	}

	public void setSuperOwner(String name) {
		this.superOwner = name;
	}

	public String getSuperOwner() {
		return superOwner;
	}

	public int getGridMinX() {
		return gridMinX;
	}

	public int getGridMaxX() {
		return gridMaxX;
	}

	public int getGridMinY() {
		return gridMinY;
	}

	public int getGridMaxY() {
		return gridMaxY;
	}

	public int getGridMinZ() {
		return gridMinZ;
	}

	public int getGridMaxZ() {
		return gridMaxZ;
	}

	public int getRadius() {
		return radius;
	}
	
	public ProtectedRegion getRegion() {
		RegionManager rm = getState().getRegionManager();
		ProtectedRegion permitRegion = rm.getRegion(getName().toLowerCase()+"_permit");
		return permitRegion;
	}
	

	public void setRegionRadius(int radius) {
		if (radius <= 3000) {
			int maxRadius = 3500;

			this.minY = 0;
			this.maxY = 256;

			int halfSize = radius / 2;
			this.radius = halfSize;

			this.minX = ((maxRadius / 2) - halfSize) + gridMinX;
			this.maxX = ((maxRadius / 2) + 1 + halfSize) + gridMinX;

			this.minZ = ((maxRadius / 2) - halfSize) + gridMinZ;
			this.maxZ = ((maxRadius / 2) + 1 + halfSize) + gridMinZ;
			
			ProtectedRegion permitRegion = getRegion();
			ProtectedRegion newRegion = new ProtectedCuboidRegion(permitRegion.getId(), new BlockVector(minX,minY,minZ), new BlockVector(maxX,maxY,maxZ));
			newRegion.setFlags(permitRegion.getFlags());
			newRegion.setMembers(permitRegion.getMembers());
			newRegion.setOwners(permitRegion.getOwners());
			newRegion.setPriority(permitRegion.getPriority());
			try {
				newRegion.setParent(permitRegion.getParent());
			} catch (CircularInheritanceException e) {
			}
			getState().getRegionManager().addRegion(newRegion);			
		}
	}
	
	public void printInfo(Player player) {
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.info.header"));
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.info.name").replaceAll("%N", getName()));
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.info.owner").replaceAll("%O", getSuperOwner()));
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.info.size").replaceAll("%S", (getRadius()*2)+"x"+(getRadius()*2)));
		
		int x = 0;
		String members = "";
		for(PermitMember member : getMembers()) {
			if(x > 0) members += ", ";
			members += member.getName();
			x++;
		}
		
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.info.members").replaceAll("%M", members));
		return;
	}

}
