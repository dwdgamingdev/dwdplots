package net.downwithdestruction.dwdplots.storage;

public enum PermitFlag {

	PLOT_CREATE("PLOT_CREATE","Create Plots", "Allows the user to create plots."),
	PLOT_MODIFY("PLOT_MODIFY","Modify Plots", "Allows the user to edit existing plots."),
	PLOT_DELETE("PLOT_DELETE","Delete Plots", "Allows the user to remove existing plots."),
	PLOT_SELL("PLOT_SELL","Sell Plots", "Allows the user to sell existing plots."),
	PERMIT_EXPAND("PERMIT_EXPAND","Expand Permit", "Allows the user to purchase more land for the permit."),
	PERMIT_BUILD("PERMIT_BUILD","Build Permissions", "Allows the user to build and modify the permit land."),
	FLAG_SET("FLAG_SET","Set Flags", "Set the flags for a permit."),
	CM_ADD("CM_ADD","Add CM","Allows the user to add Council Members"),
	CM_REMOVE("CM_REMOVE","Remove CM", "Allows the user to remove Council Members"),
	CM_FLAG("CM_FLAG","Flag CM","Allows the user to set flags for Council Members"),
	PLOT_SETPRICE("PLOT_SETPRICE", "Set Price", "Allows the user to set the price for a plot."),
	
	;
	
	private String name,ufName,help;
	
	PermitFlag(String name, String ufName, String help) {
		this.name = name;
		this.ufName = ufName;
		this.help = help;
	}
	
	PermitFlag(String name, String help) {
		this.name = name;
		this.help = help;
	}
	
	public String getName() {
		return name;
	}
	
	public String getUfName() {
		return ufName;
	}
	
	public String getHelp() {
		return help;
	}

	public static PermitFlag getEnumByName(String name) {
		for(PermitFlag flag : PermitFlag.values()) {
			if(flag.getName().equalsIgnoreCase(name)) {
				return flag;
			}
		}
		return null;
	}
}
