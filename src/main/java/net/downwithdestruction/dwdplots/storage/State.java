package net.downwithdestruction.dwdplots.storage;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;

import net.downwithdestruction.dwdplots.Coords;
import net.downwithdestruction.dwdplots.DwDPlotsPlugin;
import net.downwithdestruction.dwdplots.Util;

public class State {

	private String world;

	private File confFile;
	private YamlConfiguration conf;

	public Map<Coords, Permit> permits = new HashMap<Coords, Permit>();

	public State(String world) {
		this.world = world;
		confFile = new File("plugins/DwDPlots/states/" + world + ".yml");
		conf = YamlConfiguration.loadConfiguration(confFile);
	}

	public int getPermitCount() {
		return permits.size();
	}

	public String getName() {
		return world;
	}

	public Collection<Permit> getPermits() {
		return permits.values();
	}

	public int getNextPermitNumber() {
		DwDPlotsPlugin.debug("NextPermitNo:" + permits.size());
		return permits.size();
	}

	public Permit createPermit(String name) {
		Permit permit = new Permit(this, getNextPermitNumber(), name);
		permits.put(permit.getCoords(), permit);
		return permit;
	}

	public boolean permitExists(String name) {
		if (permits.size() > 0) {
			for (Permit permit : permits.values()) {
				if (permit.getName().equalsIgnoreCase(name)) {
					return true;
				}
			}

		}
		return false;
	}

	public Permit getPermit(String name) {
		if (permits.size() > 0) {
			for (Permit permit : permits.values()) {
				if (permit.getName().equalsIgnoreCase(name)) {
					return permit;
				}
			}
		}
		return null;
	}

	public void load() {

		try {
			Iterator<String> permitList = conf
					.getConfigurationSection("permits").getKeys(false)
					.iterator();
			while (permitList.hasNext()) {
				String key = permitList.next();
				DwDPlotsPlugin.debug("Loading Permit: "+key);
				ConfigurationSection permitSection = conf
						.getConfigurationSection("permits." + key);

				int permitNo = permitSection.getInt("permitNo");
				String superOwner = permitSection.getString("superOwner");
				int gridMinX = permitSection.getInt("grid.min.x");
				int gridMaxX = permitSection.getInt("grid.max.x");
				int gridMinY = permitSection.getInt("grid.min.y");
				int gridMaxY = permitSection.getInt("grid.max.y");
				int gridMinZ = permitSection.getInt("grid.min.z");
				int gridMaxZ = permitSection.getInt("grid.max.z");
				int minX = permitSection.getInt("region.min.x");
				int maxX = permitSection.getInt("region.max.x");
				int minY = permitSection.getInt("region.min.y");
				int maxY = permitSection.getInt("region.max.y");
				int minZ = permitSection.getInt("region.min.z");
				int maxZ = permitSection.getInt("region.max.z");
				int radius = permitSection.getInt("radius");

				Permit permit = new Permit(this, permitNo, key, superOwner,
						minX, maxX, minY, maxY, minZ, maxZ, radius, gridMinX,
						gridMaxX, gridMinY, gridMaxY, gridMinZ, gridMaxZ);

				// Members
				try {
					Iterator<String> members = permitSection

					.getConfigurationSection("members").getKeys(false)
							.iterator();
					while (members.hasNext()) {
						String member = members.next();

						// Flags
						PermitMember pm = new PermitMember(member);

						Iterator<String> flags = permitSection
								.getConfigurationSection(
										"members." + member + ".flags")
								.getKeys(false).iterator();
						while (flags.hasNext()) {
							String flag = flags.next();
							pm.addFlag(PermitFlag.getEnumByName(flag));
						}
						permit.addMember(pm);
					}
				} catch (NullPointerException e) {
					// Just means there are no members for that permit.
				}

				// Plots
				try {
					Iterator<String> plots = permitSection.getConfigurationSection("plots").getKeys(false).iterator();
					while (plots.hasNext()) {
						String plot = plots.next();
						ConfigurationSection plotSection = permitSection
								.getConfigurationSection("plots." + plot);

						Plot permitPlot = permit.createPlot(Integer.parseInt(plot));
						DwDPlotsPlugin.debug("Loading Plot: "+plot);
						
						permitPlot.setRegion(plotSection.getInt("region.min.x"), plotSection.getInt("region.min.y"), plotSection.getInt("region.min.z"), plotSection.getInt("region.max.x"), plotSection.getInt("region.max.y"), plotSection.getInt("region.max.z"));
						try {
							permitPlot.setSuperOwner(plotSection.getString("superOwner"));
						}
						catch(Exception e) {
							
						}
						permitPlot.setForSale(plotSection.getBoolean("forSale"));
						permitPlot.setPrice(plotSection.getDouble("price"));
						
						// Members
						try {
							Iterator<String> members = plotSection.getConfigurationSection("members").getKeys(false)
									.iterator();
							while (members.hasNext()) {
								String member = members.next();

								// Flags
								PlotMember pm = new PlotMember(member);

								Iterator<String> flags = plotSection
										.getConfigurationSection(
												"members." + member + ".flags")
										.getKeys(false).iterator();
								while (flags.hasNext()) {
									String flag = flags.next();
									pm.addFlag(PlotFlag.getEnumByName(flag));
								}
								permitPlot.addMember(pm);
							}
						} catch (NullPointerException e) {
							// Just means there are no members for that permit.
						}
					}
				} catch (Exception e) {
					// Just means there are no plots for this permit
				}

				permits.put(Util.getSpiralCoords(permitNo), permit);

			}
		} catch (Exception e) {

		}
	}

	public void save() {

		if (permits.size() > 0) {
			for (Permit permit : permits.values()) {
				confFile = new File("plugins/DwDPlots/states/" + world + ".yml");
				conf = YamlConfiguration.loadConfiguration(confFile);

				String permitName = permit.getName();
				conf.set("permits." + permitName + ".superOwner",
						permit.getSuperOwner());
				conf.set("permits." + permitName + ".permitNo",
						permit.getPermitNo());
				conf.set("permits." + permitName + ".grid.min.x",
						permit.getGridMinX());
				conf.set("permits." + permitName + ".grid.min.y",
						permit.getGridMinY());
				conf.set("permits." + permitName + ".grid.min.z",
						permit.getGridMinZ());
				conf.set("permits." + permitName + ".grid.max.x",
						permit.getGridMaxX());
				conf.set("permits." + permitName + ".grid.max.y",
						permit.getGridMaxY());
				conf.set("permits." + permitName + ".grid.max.z",
						permit.getGridMaxZ());
				conf.set("permits." + permitName + ".region.min.x",
						permit.getMinX());
				conf.set("permits." + permitName + ".region.min.y",
						permit.getMinY());
				conf.set("permits." + permitName + ".region.min.z",
						permit.getMinZ());
				conf.set("permits." + permitName + ".region.max.x",
						permit.getMaxX());
				conf.set("permits." + permitName + ".region.max.y",
						permit.getMaxY());
				conf.set("permits." + permitName + ".region.max.z",
						permit.getMaxZ());
				conf.set("permits." + permitName + ".radius",
						permit.getRadius());

				for (PermitMember pm : permit.getMembers()) {

					for (PermitFlag flag : pm.getFlags()) {
						conf.set(
								"permits." + permitName + ".members."
										+ pm.getName() + ".flags."
										+ flag.getName(), true);
					}
				}
				
				for(Plot plot : permit.getPlots()) {
					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".superOwner", plot.getSuperOwner());
					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".price", plot.getPrice());

					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".region.min.x", plot.getMinX());
					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".region.min.y", plot.getMinY());
					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".region.min.z", plot.getMinZ());
					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".region.max.x", plot.getMaxX());
					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".region.max.y", plot.getMaxY());
					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".region.max.z", plot.getMaxZ());
					conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + ".forSale", plot.isForSale());
					
					for(PlotMember pm : plot.getMembers()) {
						for(PlotFlag flag : pm.getFlags()) {
							conf.set("permits." + permitName + ".plots." + plot.getPlotNo() + "." + pm.getName() + ".flags." + flag.getName(), true);
						}
					}
				}
			}
		}
		try {
			conf.save(confFile);
		} catch (IOException e) {
			DwDPlotsPlugin.log("Unable to save State: " + world);
		}
	}

	public Permit getPermitFromFlag(Player player, PermitFlag flag) {
		for (Permit permit : permits.values()) {
			if (permit.memberHas(player.getName(), flag)
					|| permit.isSuperOwner(player.getName())) {
				return permit;
			}
		}
		return null;
	}

	public Permit getPermitFromPlayer(Player player) {
		for (Permit permit : permits.values()) {
			if (permit.isMember(player.getName())
					|| permit.isSuperOwner(player.getName())) {
				return permit;
			}
		}
		return null;
	}

	public Permit getPermitFromLocation(Location loc) {
		for (Permit permit : permits.values()) {
			if (permit.containsLocation(loc)) {
				return permit;
			}
		}
		return null;
	}
	
	public RegionManager getRegionManager() {
		WorldGuardPlugin wg = DwDPlotsPlugin.instance.getWorldGuard();
		RegionManager rm = wg.getRegionManager(Bukkit.getWorld(getName()));
		return rm;
	}
	
	public void printInfo(Player player) {
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.state.info.header"));
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.state.info.name").replaceAll("%N", getName()));
		
		//int x = 0;
		String flagList = "Coming Soon!";
		/*for(StateFlag flag : state.getFlags()) {
			if(x > 0) flagList += ", ";
			flagList += flag.getName();
			x++;
		}*/
		
		int x = 0;
		String permits = "";
		for(Permit permit : getPermits()) {
			if(x > 0) permits += ", ";
			permits += permit.getName();
			x++;
		}
		
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.state.info.flags").replaceAll("%F", flagList));
		player.sendMessage(DwDPlotsPlugin.getLang().get("commands.state.info.permits").replaceAll("%N", getName()).replaceAll("%P", permits));
	}

	public void deletePermit(Permit permit) {
		if(permits.containsValue(permit)) {
			permits.remove(permit.getCoords());
		}
	}

}
