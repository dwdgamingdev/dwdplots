package net.downwithdestruction.dwdplots.storage;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class States {

	private static Map<String,State> states = new HashMap<String,State>();
	
	public static State getState(World world) {
		return getState(world.getName());
	}
	
	public static State getState(String world) {
		if(states.containsKey(world)) {
			return states.get(world);
		}
		else {
			return new State(world);
		}
	}
	
	public static Permit getPermitFromFlag(Player player, PermitFlag flag) {
		for(State state : states.values()) {
			if(state.getPermitFromFlag(player, flag) != null) {
				return state.getPermitFromFlag(player, flag);
			}
		}
		return null;
	}
	
	public static Permit getPermitFromPlayer(Player player) {
		for(State state : states.values()) {
			if(state.getPermitFromPlayer(player) != null) {
				return state.getPermitFromPlayer(player);
			}
		}
		return null;
	}
	
	public static Permit getPermitFromLocation(Location loc) {
		State state = getStateFromLocation(loc);
		if(state != null) {
			return state.getPermitFromLocation(loc);
		}
		return null;
	}
	
	public static Plot getPlotFromLocation(Location loc) {
		if(getState(loc.getWorld()) != null) {
			State state = getState(loc.getWorld());
			Permit permit = state.getPermitFromLocation(loc);
			if(permit != null) {
				Plot plot = permit.getPlotFromLocation(loc);
				if(plot != null) {
					return plot;
				}
			}
		}
		return null;
	}
	
	public static State createState(String world) {
		if(!states.containsKey(world)) {
			State state = new State(world);
			states.put(world,state);
			return state;
		}
		else {
			return getState(world);
		}
	}
	
	public static void loadStates() {
		File[] files = new File("plugins/DwDPlots/states").listFiles();
		for (File file : files) {
			String name = file.getName();
			int pos = name.lastIndexOf('.');
			String ext = name.substring(pos + 1);
			if (ext.equalsIgnoreCase("yml")) {
				name = name.replaceAll(".yml", "");
				State state = new State(name);
				state.load();
				states.put(name, state);
			}
		}
	}
	
	public static void saveStates() {
		for(State state : states.values()) {
			state.save();
		}
	}
	
	public static Collection<State> getStates() {
		return states.values();
	}

	public static State getStateFromLocation(Location loc) {
		if(states.containsKey(loc.getWorld().getName())) {
			return states.get(loc.getWorld().getName());
		}
		return null;
	}

	public static Permit getPermitFromName(String name) {
		for(State state : states.values()) {
			if(state.getPermit(name) != null) {
				return state.getPermit(name);
			}
		}
		return null;
	}

}
