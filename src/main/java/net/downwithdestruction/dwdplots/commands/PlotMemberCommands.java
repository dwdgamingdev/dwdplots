package net.downwithdestruction.dwdplots.commands;

import net.downwithdestruction.dwdplots.DwDPlotsPlugin;
import net.downwithdestruction.dwdplots.storage.Permit;
import net.downwithdestruction.dwdplots.storage.Plot;
import net.downwithdestruction.dwdplots.storage.PlotFlag;
import net.downwithdestruction.dwdplots.storage.PlotMember;
import net.downwithdestruction.dwdplots.storage.States;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class PlotMemberCommands {

	@Command(aliases = { "list", "l" }, usage = "", flags = "", desc = "List current members", help = "[page]", min = 0, max = 1)
	@CommandPermissions("dwdplots.plot.members.list")
	public static void list(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPlotFromLocation(player.getLocation()) != null) {
			Plot plot = States.getPlotFromLocation(player.getLocation());
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"commands.plot.member.list.header"));

			int x = 0;
			String members = "";
			for (PlotMember member : plot.getMembers()) {
				if (x > 0)
					members += DwDPlotsPlugin.getLang().get(
							"commands.plot.member.list.seperator");
				members += member.getName();
			}
			sender.sendMessage(DwDPlotsPlugin.getLang()
					.get("commands.plot.member.list.item")
					.replaceAll("%L", members));

		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.notInsidePlot"));
		}
	}

	@Command(aliases = { "add", "a" }, usage = "", flags = "", desc = "Add a member", help = "<Player>", min = 1, max = 1)
	@CommandPermissions("dwdplots.plot.members.add")
	public static void add(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPlotFromLocation(player.getLocation()) != null) {
			Plot plot = States.getPlotFromLocation(player.getLocation());
			if (plot.memberHas(player.getName(), PlotFlag.MEMBERS_ADD)) {
				PlotMember pm = new PlotMember(args.getString(0));
				plot.addMember(pm);
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"commands.plot.member.add"));
			} else {
				sender.sendMessage(DwDPlotsPlugin.getLang()
						.get("exceptions.missingFlag")
						.replaceAll("%F", "MEMBERS_ADD"));
			}

		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.notInsidePlot"));
		}
	}

	@Command(aliases = { "remove", "rm" }, usage = "", flags = "", desc = "Remove a current member", help = "<Player>", min = 1, max = 1)
	@CommandPermissions("dwdplots.plot.members.remove")
	public static void remove(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPlotFromLocation(player.getLocation()) != null) {
			Plot plot = States.getPlotFromLocation(player.getLocation());
			if (plot.memberHas(player.getName(), PlotFlag.MEMBERS_REMOVE)) {
				plot.removeMember(args.getString(0));
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"commands.plot.member.remove"));

			} else {
				sender.sendMessage(DwDPlotsPlugin.getLang()
						.get("exceptions.missingFlag")
						.replaceAll("%F", "MEMBERS_REMOVE"));
			}

		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.notInsidePlot"));
		}
	}

	@Command(aliases = { "role", "r" }, usage = "", flags = "", desc = "Modify a role for a member", help = "<Player> <Role> [value] (Blank to remove)", min = 2, max = -1)
	@CommandPermissions("dwdplots.plot.members.role")
	public static void flag(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPlotFromLocation(player.getLocation()) != null) {
			Plot plot = States.getPlotFromLocation(player.getLocation());
			if (plot.memberHas(player.getName(), PlotFlag.MEMBERS_MODIFY)) {
				String membername = args.getString(0);
				if (plot.isMember(membername)) {
					PlotFlag flag = PlotFlag.getEnumByName(args.getString(1));
					PlotMember member = plot.getMember(membername);
					Permit permit = plot.getPermit();

					if (args.argsLength() > 2) {
						
						if(flag == PlotFlag.BUILD) {
							if(!plot.getRegion().isMember(membername)) {
								plot.getRegion().getMembers().addPlayer(membername);
							}
						}
						
						member.addFlag(flag);
						sender.sendMessage(DwDPlotsPlugin.getLang()
								.get("commands.plot.member.role.add")
								.replaceAll("%N", membername)
								.replaceAll("%P", permit.getName())
								.replaceAll("%I", "" + plot.getPlotNo())
								.replaceAll("%F", flag.getName()));
					} else {
						if(flag == PlotFlag.BUILD) {
							if(!plot.getRegion().isMember(membername)) {
								plot.getRegion().getMembers().removePlayer(membername);
							}
						}
						member.removeFlag(flag);
						sender.sendMessage(DwDPlotsPlugin.getLang()
								.get("commands.plot.member.role.remove")
								.replaceAll("%N", membername)
								.replaceAll("%P", permit.getName())
								.replaceAll("%I", "" + plot.getPlotNo())
								.replaceAll("%F", flag.getName()));
					}
				} else {
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"exceptions.playerNotMemberOfPlot"));
				}

			} else {
				sender.sendMessage(DwDPlotsPlugin.getLang()
						.get("exceptions.missingFlag")
						.replaceAll("%F", "MEMBERS_MODIFY"));
			}
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.notInsidePlot"));
		}
	}

	@Command(aliases = { "roles", "rs" }, usage = "", flags = "", desc = "List all available roles", help = "[page]", min = 0, max = 1)
	@CommandPermissions("dwdplots.plot.members.roles")
	public static void flags(CommandContext args, CommandSender sender)
			throws CommandException {

		PlotFlag[] flags = PlotFlag.values();
		final int totalSize = flags.length;
		final int pageSize = 6;
		final int pages = (int) Math.ceil(totalSize / (float) pageSize);

		int page = args.getInteger(0, 1) - 1;

		if (page < 0) {
			page = 0;
		}

		int start = page * pageSize;
		int end = start + pageSize - 1;

		if (page < pages) {
			sender.sendMessage(DwDPlotsPlugin.getLang()
					.get("commands.plot.member.roles.header")
					.replaceAll("%P", "" + (page + 1))
					.replaceAll("%T", "" + pages));
			int x = 0;
			for (PlotFlag flag : flags) {
				if (x >= start && x <= end) {
					sender.sendMessage(DwDPlotsPlugin.getLang()
							.get("commands.plot.member.roles.item")
							.replaceAll("%N", flag.getName())
							.replaceAll("%I", "" + flag.getHelp()));
				}
				x++;
			}
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.invalidPage"));
		}
	}

	@Command(aliases = { "info", "i" }, usage = "", flags = "", desc = "Show information on a member", help = "<Player>", min = 1, max = 1)
	@CommandPermissions("dwdplots.plot.members.info")
	public static void info(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPlotFromLocation(player.getLocation()) != null) {
			Plot plot = States.getPlotFromLocation(player.getLocation());
			if (plot.isMember(player.getName())) {
				String playername = args.getString(0);

				String name = plot.getSuperOwner();
				PlotFlag[] flags = PlotFlag.values();
				
				if(!plot.isSuperOwner(playername)) {
					PlotMember member = plot.getMember(playername);
				
					name = member.getName();
					flags = member.getFlags();
				}
				
				int x = 0;
				String flagList = "";
				for(PlotFlag flag : flags) {
					if(x > 0) flagList += ", ";
					flagList += flag.getUfName();
					x++;
				}
				
				sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.plot.member.info.header"));
				sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.plot.member.info.name").replaceAll("%N",name));
				sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.plot.member.info.flags").replaceAll("%F",flagList));
			} else {
				sender.sendMessage(DwDPlotsPlugin.getLang()
						.get("exceptions.notMemberOfPlot"));
			}

		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.notInsidePlot"));
		}
	}

}
