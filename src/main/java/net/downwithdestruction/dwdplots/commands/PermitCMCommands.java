package net.downwithdestruction.dwdplots.commands;

import net.downwithdestruction.dwdplots.DwDPlotsPlugin;
import net.downwithdestruction.dwdplots.storage.Permit;
import net.downwithdestruction.dwdplots.storage.PermitFlag;
import net.downwithdestruction.dwdplots.storage.PermitMember;
import net.downwithdestruction.dwdplots.storage.States;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class PermitCMCommands {

	@Command(aliases = { "list", "l" }, usage = "", flags = "", desc = "List current Council Members", help = "", min = 0, max = 0)
	@CommandPermissions("dwdplots.permit.cm.list")
	public static void list(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromPlayer(player) != null) {
			Permit permit = States.getPermitFromPlayer(player);
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"commands.permit.cm.list.header"));

			int x = 0;
			String members = "";
			for (PermitMember member : permit.getMembers()) {
				if (x > 0)
					members += ", ";
				members += member.getName();
			}
			sender.sendMessage(DwDPlotsPlugin.getLang()
					.get("commands.permit.cm.list.item")
					.replaceAll("%L", members));

		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.doesntOwnPermit"));
		}
	}

	@Command(aliases = { "add", "a" }, usage = "", flags = "", desc = "Add a Council Member", help = "<Player>", min = 1, max = 1)
	@CommandPermissions("dwdplots.permit.cm.add")
	public static void add(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromPlayer(player) != null) {
			Permit permit = States.getPermitFromPlayer(player);
			if (permit.memberHas(player.getName(), PermitFlag.CM_ADD)) {
				String membername = args.getString(0);
				PermitMember member = new PermitMember(membername);
				permit.addMember(member);
				sender.sendMessage(DwDPlotsPlugin.getLang()
						.get("commands.permit.cm.add")
						.replaceAll("%N", membername)
						.replaceAll("%P", permit.getName()));
			} else {
				sender.sendMessage(DwDPlotsPlugin.getLang()
						.get("exceptions.missingFlag")
						.replaceAll("%F", "CM_ADD"));
			}
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.doesntOwnPermit"));
		}
	}

	@Command(aliases = { "remove", "rm" }, usage = "", flags = "", desc = "Remove a current Council Member", help = "<Player>", min = 1, max = 1)
	@CommandPermissions("dwdplots.permit.cm.remove")
	public static void remove(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromPlayer(player) != null) {
			Permit permit = States.getPermitFromPlayer(player);
			if (permit.memberHas(player.getName(), PermitFlag.CM_REMOVE)) {
				String membername = args.getString(0);
				if (permit.isMember(membername)) {
					permit.removeMember(membername);
					sender.sendMessage(DwDPlotsPlugin.getLang()
							.get("commands.permit.cm.add")
							.replaceAll("%N", membername)
							.replaceAll("%P", permit.getName()));
				} else {
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"exceptions.playerNotMemberOfPermit"));
				}
			} else {
				sender.sendMessage(DwDPlotsPlugin.getLang()
						.get("exceptions.missingFlag")
						.replaceAll("%F", "CM_REMOVE"));
			}
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.doesntOwnPermit"));
		}
	}

	@Command(aliases = { "role", "r" }, usage = "", flags = "", desc = "Modify a role for a council member", help = "<Player> <Role> [value] (Blank to remove)", min = 2, max = -1)
	@CommandPermissions("dwdplots.permit.cm.role")
	public static void role(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromPlayer(player) != null) {
			Permit permit = States.getPermitFromPlayer(player);
			if (permit.memberHas(player.getName(), PermitFlag.CM_FLAG)) {
				String membername = args.getString(0);
				if (permit.isMember(membername)) {

					if (PermitFlag.getEnumByName(args.getString(1)) != null) {
						PermitFlag flag = PermitFlag.getEnumByName(args
								.getString(1));

						PermitMember member = permit.getMember(membername);

						if (args.argsLength() > 2) {
							
							if(flag == PermitFlag.PERMIT_BUILD) {
								if(!permit.getRegion().isMember(membername)) {
									permit.getRegion().getMembers().addPlayer(membername);
								}
							}
							
							member.addFlag(flag);
							sender.sendMessage(DwDPlotsPlugin.getLang()
									.get("commands.permit.cm.flag.add")
									.replaceAll("%N", membername)
									.replaceAll("%P", permit.getName())
									.replaceAll("%F", flag.getName()));
						} else {
							
							if(flag == PermitFlag.PERMIT_BUILD) {
								if(!permit.getRegion().isMember(membername)) {
									permit.getRegion().getMembers().addPlayer(membername);
								}
							}
							
							member.removeFlag(flag);
							sender.sendMessage(DwDPlotsPlugin.getLang()
									.get("commands.permit.cm.flag.remove")
									.replaceAll("%N", membername)
									.replaceAll("%P", permit.getName())
									.replaceAll("%F", flag.getName()));
						}
					}
					else {
						sender.sendMessage(DwDPlotsPlugin.getLang().get(
								"exceptions.invalidFlag"));
					}
				} else {
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"exceptions.playerNotMemberOfPermit"));
				}
			} else {
				sender.sendMessage(DwDPlotsPlugin.getLang()
						.get("exceptions.missingFlag")
						.replaceAll("%F", "CM_FLAG"));
			}
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.doesntOwnPermit"));
		}
	}

	@Command(aliases = { "roles", "rs" }, usage = "", flags = "", desc = "List all available roles", help = "[page]", min = 0, max = 1)
	@CommandPermissions("dwdplots.permit.cm.roles")
	public static void roles(CommandContext args, CommandSender sender)
			throws CommandException {
		PermitFlag[] flags = PermitFlag.values();
		final int totalSize = flags.length;
		final int pageSize = 6;
		final int pages = (int) Math.ceil(totalSize / (float) pageSize);

		int page = args.getInteger(0, 1) - 1;

		if (page < 0) {
			page = 0;
		}

		int start = page * pageSize;
		int end = start + pageSize - 1;

		if (page < pages) {
			sender.sendMessage(DwDPlotsPlugin.getLang()
					.get("commands.permit.flags.header")
					.replaceAll("%P", "" + (page + 1))
					.replaceAll("%T", "" + pages));
			int x = 0;
			for (PermitFlag flag : flags) {
				if (x >= start && x <= end) {
					sender.sendMessage(DwDPlotsPlugin.getLang()
							.get("commands.permit.flags.item")
							.replaceAll("%N", flag.getName())
							.replaceAll("%I", "" + flag.getHelp()));
				}
				x++;
			}
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.invalidPage"));
		}
	}

	@Command(aliases = { "info", "i" }, usage = "", flags = "", desc = "Show information on a council member", help = "<Player>", min = 1, max = 1)
	@CommandPermissions("dwdplots.permit.cm.info")
	public static void info(CommandContext args, CommandSender sender)
			throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromPlayer(player) != null) {
			Permit permit = States.getPermitFromPlayer(player);
			String playername = args.getString(0);

			String name = permit.getSuperOwner();
			PermitFlag[] flags = PermitFlag.values();
			
			if(!permit.isSuperOwner(playername)) {
				PermitMember member = permit.getMember(playername);
			
				name = member.getName();
				flags = member.getFlags();
			}
			
			int x = 0;
			String flagList = "";
			for(PermitFlag flag : flags) {
				if(x > 0) flagList += ", ";
				flagList += flag.getUfName();
				x++;
			}
			
			sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.cm.info.header"));
			sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.cm.info.name").replaceAll("%N",name));
			sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.cm.info.flags").replaceAll("%F",flagList));
			
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.userDoesntOwnPermit"));
		}
	}

}
