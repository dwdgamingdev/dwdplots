package net.downwithdestruction.dwdplots.commands;

import net.downwithdestruction.dwdplots.DwDPlotsPlugin;
import net.downwithdestruction.dwdplots.PlayerData;
import net.downwithdestruction.dwdplots.Util;
import net.downwithdestruction.dwdplots.storage.Permit;
import net.downwithdestruction.dwdplots.storage.PermitFlag;
import net.downwithdestruction.dwdplots.storage.Plot;
import net.downwithdestruction.dwdplots.storage.PlotFlag;
import net.downwithdestruction.dwdplots.storage.PlotMember;
import net.downwithdestruction.dwdplots.storage.States;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.NestedCommand;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.databases.ProtectionDatabaseException;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion.CircularInheritanceException;

public class PlotCommands {
	
	@Command(aliases = { "create", "c" },usage = "", flags = "", desc = "Create a new plot.",help = "[PlotID]", min = 0, max = 1)
	@CommandPermissions("dwdplots.plot.create")
	public static void create(CommandContext args, CommandSender sender) throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromFlag(player,PermitFlag.PLOT_CREATE) != null) {
			Permit permit = States.getPermitFromFlag(player,PermitFlag.PLOT_CREATE);
			int plotID = permit.getNextPlotNumber();
			
			if(args.argsLength() > 0) {
				if(permit.getPlot(args.getInteger(0)) == null) {
					plotID = args.getInteger(0);
				}
				else{
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"commands.plot.create.notCreated"));
					return;
				}
			}
			
			WorldEditPlugin we = DwDPlotsPlugin.instance.getWorldEdit();
			
	        Selection sel = we.getSelection(player);
	        if(sel == null){
	            player.sendMessage(DwDPlotsPlugin.getLang().get(
						"exceptions.requiresWorldEditSelection"));
	            return;
	        }
	        sel.getNativeMaximumPoint();
	        Location max = sel.getMaximumPoint();
	        Location min = sel.getMinimumPoint();
			
			Plot plot = permit.createPlot(plotID);
			plot.setRegion(min.getBlockX(), min.getBlockY(), min.getBlockZ(), max.getBlockX(), max.getBlockY(), max.getBlockZ());
			plot.setPrice(0.00);
			
			RegionManager rm = permit.getState().getRegionManager();
			
			
			ProtectedRegion permitRegion = plot.getPermit().getRegion();
			ProtectedRegion plotRegion = new ProtectedCuboidRegion(permit.getName()+"_plot"+plotID, 
					new BlockVector(min.getBlockX(),min.getBlockY(),min.getBlockZ()), 
					new BlockVector(max.getBlockX(),max.getBlockY(),max.getBlockZ()));
			
			if(!(permitRegion.contains(plotRegion.getMinimumPoint()) && permitRegion.contains(plotRegion.getMaximumPoint()))) {
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"exceptions.plotSelectionMustBeInPermit"));
				return;
			}
			
			rm.addRegion(plotRegion);
			plotRegion = plot.getRegion();
			
			try {
				plotRegion.setParent(permitRegion);
			} catch (CircularInheritanceException e) {
				e.printStackTrace();
			}
			
			try {
				rm.save();
			} catch (ProtectionDatabaseException e) {
				e.printStackTrace();
			}
			
			
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"commands.plot.create.created").replaceAll("%I", ""+plotID));
			
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.doesntOwnPermit"));
		}
	}
	
	@Command(aliases = { "delete", "d", "remove", "r" },usage = "", flags = "", desc = "Remove a plot.",help = "<PlotID>", min = 1, max = 1)
	@CommandPermissions("dwdplots.plot.delete")
	public static void delete(CommandContext args, CommandSender sender) throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromFlag(player,PermitFlag.PLOT_DELETE) != null) {
			Permit permit = States.getPermitFromFlag(player,PermitFlag.PLOT_DELETE);
			if(permit.getPlot(args.getInteger(0)) == null) {
				Plot plot = permit.getPlot(args.getInteger(0));
				plot.getPermit().getState().getRegionManager().removeRegion(plot.getRegion().getId());
				permit.deletePlot(args.getInteger(0));
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"commands.plot.delete.deleted"));
			}
			else{
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"commands.plot.delete.doesntExist"));
				return;
			}
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.doesntOwnPermit"));
		}
	}
	
	@Command(aliases = { "info", "i" },usage = "", flags = "", desc = "Show information on a plot",help = "[plotID]", min = 0, max = 1)
	@CommandPermissions("dwdplots.plot.info")
	public static void info(CommandContext args, CommandSender sender) throws CommandException {
		Plot plot = null;
		if(args.argsLength() > 0) {
			plot = States.getPermitFromLocation(Bukkit.getPlayerExact(sender.getName()).getLocation()).getPlot(args.getInteger(0));
			if(plot == null) {
				sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.plotDoesntExist"));
				return;
			}
		}
		else {
			plot = States.getPlotFromLocation(Bukkit.getPlayerExact(sender.getName()).getLocation());
			if(plot == null) {
				sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.notInsidePlot"));
				return;
			}
		}
		
		plot.printInfo((Player) sender);
	}
	
	@Command(aliases = { "list", "l" },usage = "", flags = "", desc = "List all plots in your permit",help = "[page]", min = 0, max = 1)
	@CommandPermissions("dwdplots.plot.list")
	public static void list(CommandContext args, CommandSender sender) throws CommandException {
		
	}
	
	@Command(aliases = { "setprice", "price", "p" },usage = "", flags = "", desc = "Set the price of a plot",help = "<PlotID> <Price>", min = 1, max = 2)
	@CommandPermissions("dwdplots.plot.setprice")
	public static void setprice(CommandContext args, CommandSender sender) throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromFlag(player,PermitFlag.PLOT_SETPRICE) != null) {
			Permit permit = States.getPermitFromFlag(player,PermitFlag.PLOT_SETPRICE);
			if(permit.getPlot(args.getInteger(0)) != null) {
				Plot plot = permit.getPlot(args.getInteger(0));
				if(plot.setPrice(args.getDouble(1)) == true) {
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"commands.plot.setprice.set").replaceAll("%P", ""+plot.getPrice()).replaceAll("%N", ""+plot.getPlotNo()));
				}
				else {
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"commands.plot.setprice.setDefault").replaceAll("%P", ""+plot.getPrice()).replaceAll("%N", ""+plot.getPlotNo()));
				}
			}
			else {
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"exceptions.plotDoesntExist"));
			}
		}
		else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.requestTimeout"));
		}
	}
	
	@Command(aliases = { "flag", "f" },usage = "", flags = "", desc = "Modify flags for a plot",help = "<Flag> [value] (Blank to reset)", min = 1, max = -1)
	@CommandPermissions("dwdplots.plot.flag")
	public static void flag(CommandContext args, CommandSender sender) throws CommandException {
		
	}
	
	@Command(aliases = { "flags", "fs" },usage = "", flags = "", desc = "List all available flags.",help = "[page]", min = 0, max = 1)
	@CommandPermissions("dwdplots.plot.flags")
	public static void flags(CommandContext args, CommandSender sender) throws CommandException {
		
	}
	
	@Command(aliases = { "sell", "s" },usage = "", flags = "", desc = "Offer a plot to a member.",help = "<Player> [PlotID]", min = 1, max = 2)
	@CommandPermissions("dwdplots.plot.sell")
	public static void sell(CommandContext args, CommandSender sender) throws CommandException {
		Player player = (Player) sender;
		if (States.getPermitFromFlag(player,PermitFlag.PLOT_SELL) != null) {
			Permit permit = States.getPermitFromFlag(player,PermitFlag.PLOT_SELL);
			// Check if player is online
			if(Bukkit.getPlayer(args.getString(0)) != null) {
				if(permit.getPlot(args.getInteger(1)) != null) {
					Plot plot = permit.getPlot(args.getInteger(1));
					Player client = Bukkit.getPlayer(args.getString(0));
					PlayerData dat = new PlayerData(client.getName());
					dat.setPlotConfirmMode(args.getInteger(1));
					dat.setPermit(permit);
					dat.setSender(player);
					Util.addPlayerData(dat);
					client.sendMessage(DwDPlotsPlugin.getLang().get(
							"commands.plot.sell.offer1").replaceAll("%P", sender.getName()).replaceAll("%I", args.getString(1)).replaceAll("%N", permit.getName()).replaceAll("%C", ""+plot.getPrice()));
					client.sendMessage(DwDPlotsPlugin.getLang().get(
							"commands.plot.sell.offer2").replaceAll("%P", sender.getName()).replaceAll("%I", args.getString(1)).replaceAll("%N", permit.getName()).replaceAll("%C", ""+plot.getPrice()));
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"commands.plot.sell.awaiting"));
				}
				else {
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"exceptions.plotDoesntExist"));
				}
			}
			else {
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"exceptions.playerNotOnline"));
			}
			
		} else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get(
					"exceptions.doesntOwnPermit"));
		}
	}
	
	@Command(aliases = { "confirm", "yes" },usage = "", flags = "", desc = "Confirm a plot sale.",help = "", min = 0, max = 0)
	@CommandPermissions("dwdplots.plot.confirm")
	public static void confirm(CommandContext args, CommandSender sender) throws CommandException {
		if(Util.getPlayerData(sender.getName()) != null) {
			PlayerData pd = Util.getPlayerData(sender.getName());
			if(pd.checkConfirm() == true) {
				Permit permit = pd.getPermit();
				Plot plot = permit.getPlot(pd.getConfirmationNumber());
				
				double price = plot.getPrice();
				if(DwDPlotsPlugin.economy.has(sender.getName(), price)) {
					DwDPlotsPlugin.economy.withdrawPlayer(sender.getName(), price);
					DwDPlotsPlugin.economy.depositPlayer(permit.getSuperOwner(), price);
					plot.setSuperOwner(sender.getName());
					plot.setForSale(false);
					
					WorldGuardPlugin wg = DwDPlotsPlugin.instance.getWorldGuard();
					RegionManager rm = wg.getRegionManager(Bukkit.getWorld(permit.getState().getName()));
					ProtectedRegion region = rm.getRegion(permit.getName()+"_plot"+plot.getPlotNo());
					
					DefaultDomain members = new DefaultDomain();
					members.addPlayer(sender.getName());
					
					for(PlotMember member : plot.getMembers()) {
						if(member.hasFlag(PlotFlag.BUILD)) {
							members.addPlayer(member.getName());
						}
					}
					
					region.setMembers(members);
					
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"commands.plot.confirm.client"));
					
					pd.getSender().sendMessage(DwDPlotsPlugin.getLang().get(
							"commands.plot.confirm.sender"));
				}
				else {
					sender.sendMessage(DwDPlotsPlugin.getLang().get(
							"exceptions.notEnoughFunds"));
				}
				
				Util.removePlayerData(sender.getName());
			}
			else {
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"exceptions.requestTimeout"));
			}
		}
	}
	
	@Command(aliases = { "deny", "no" },usage = "", flags = "", desc = "Deny a plot sale.",help = "", min = 0, max = 0)
	@CommandPermissions("dwdplots.plot.deny")
	public static void deny(CommandContext args, CommandSender sender) throws CommandException {
		if(Util.getPlayerData(sender.getName()) != null) {
			PlayerData pd = Util.getPlayerData(sender.getName());
			if(pd.checkConfirm() == true) {
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"commands.plot.deny.client"));
				pd.getSender().sendMessage(DwDPlotsPlugin.getLang().get(
						"commands.plot.deny.sender"));
				Util.removePlayerData(sender.getName());
			}
			else {
				sender.sendMessage(DwDPlotsPlugin.getLang().get(
						"exceptions.requestTimeout"));
			}
		}
	}
	
	@Command(aliases = { "members", "m" },usage = "", flags = "", desc = "Modify Plot Members",help = "[plotID]", min = 0, max = 1)
	@NestedCommand(value = { PlotMemberCommands.class })
	public static void members(CommandContext args, CommandSender sender) throws CommandException {
	}
	
	
	
}
