package net.downwithdestruction.dwdplots.commands;


import org.bukkit.command.CommandSender;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.NestedCommand;

public class GeneralCommands {
	@Command(aliases = { "permit" },usage = "", flags = "", desc = "Permit commands.",help = "All commands related to Permits.", min = 0, max = 0)
	@NestedCommand(value = { PermitCommands.class })
	public static void permit(CommandContext args, CommandSender sender) throws CommandException {
	}
	
	@Command(aliases = { "state" },usage = "", flags = "", desc = "State commands.",help = "All commands related to States.", min = 0, max = 0)
	@NestedCommand(value = { StateCommands.class })
	public static void state(CommandContext args, CommandSender sender) throws CommandException {
	}
	
	@Command(aliases = { "plot" },usage = "", flags = "", desc = "Plot commands.",help = "All commands related to Plots.", min = 0, max = 0)
	@NestedCommand(value = { PlotCommands.class })
	public static void plot(CommandContext args, CommandSender sender) throws CommandException {
	}
}
