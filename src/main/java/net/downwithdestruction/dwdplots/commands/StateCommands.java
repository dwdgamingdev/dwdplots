package net.downwithdestruction.dwdplots.commands;

import java.util.Collection;

import net.downwithdestruction.dwdplots.DwDPlotsPlugin;
import net.downwithdestruction.dwdplots.storage.State;
import net.downwithdestruction.dwdplots.storage.States;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;

public class StateCommands {
	@Command(aliases = { "create", "c" },usage = "", flags = "", desc = "Create a new State.",help = "<World>", min = 1, max = 1)
	@CommandPermissions("dwdplots.state.create")
	public static void create(CommandContext args, CommandSender sender) throws CommandException {
		States.createState(args.getString(0));
		sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.state.create.created"));
	}
	
	@Command(aliases = { "info", "i" },usage = "", flags = "", desc = "Get information on a State.",help = "<State>", min = 1, max = 1)
	@CommandPermissions("dwdplots.state.info")
	public static void info(CommandContext args, CommandSender sender) throws CommandException {
		if(States.getState(args.getString(0)) != null) {
			State state = States.getState(args.getString(0));
			state.printInfo((Player) sender);
		}
		else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.stateDoesntExist"));
		}
	}
	
	@Command(aliases = { "list", "l" },usage = "", flags = "", desc = "List all states.",help = "[page]", min = 0, max = 1)
	@CommandPermissions("dwdplots.state.list")
	public static void list(CommandContext args, CommandSender sender) throws CommandException {
		
		Collection<State> states = States.getStates();		
		final int totalSize = states.size();
		final int pageSize = 6;
		final int pages = (int) Math.ceil(totalSize / (float) pageSize);
		
		int page = args.getInteger(0, 1) - 1;

		if (page < 0) {
			page = 0;
		}
		
		int start = page * pageSize;
		int end = start+pageSize-1;
		

		if (page < pages) {
			sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.state.list.header").replaceAll("%P", ""+(page+1)).replaceAll("%T", ""+pages));
			int x = 0;
			for(State state : states) {
				if(x >= start && x <= end) {
					sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.state.list.item").replaceAll("%N", state.getName()).replaceAll("%P",""+state.getPermitCount()));
				}
				x++;
			}
		}
		else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.invalidPage"));
		}
	}
}
