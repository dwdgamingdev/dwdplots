package net.downwithdestruction.dwdplots.commands;

import java.util.Collection;

import net.downwithdestruction.dwdplots.DwDPlotsPlugin;
import net.downwithdestruction.dwdplots.storage.Permit;
import net.downwithdestruction.dwdplots.storage.PermitFlag;
import net.downwithdestruction.dwdplots.storage.PermitMember;
import net.downwithdestruction.dwdplots.storage.State;
import net.downwithdestruction.dwdplots.storage.States;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.NestedCommand;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion.CircularInheritanceException;

public class PermitCommands {
	
	@Command(aliases = { "create", "c" },usage = "", flags = "", desc = "Create a new permit.",help = "<Permit Name> [world]", min = 1, max = 2)
	@CommandPermissions("dwdplots.permit.create")
	public static void create(CommandContext args, CommandSender sender) throws CommandException {
		
		// <Permit Name> [world]
		Player player = (Player) sender;
		World world = player.getWorld();
		
		if(args.argsLength() > 1) {
			world = Bukkit.getWorld(args.getString(1));
		}
		
		// get where it will go
		State state = States.getState(world);
		Permit permit = state.createPermit(args.getString(0));
		
		// create WorldGuard Region
		RegionManager rm = state.getRegionManager();
		
		// Absolute Maximum Region Space
		ProtectedRegion parent = new ProtectedCuboidRegion(args.getString(0).toLowerCase()+"_grid",new BlockVector(permit.getGridMinX(),permit.getGridMinY(),permit.getGridMinZ()),new BlockVector(permit.getGridMaxX(),permit.getGridMaxY(),permit.getGridMaxZ()));
		rm.addRegion(parent);
		
		// Actual Permit
		ProtectedRegion region = new ProtectedCuboidRegion(args.getString(0).toLowerCase()+"_permit",new BlockVector(permit.getMinX(),permit.getMinY(),permit.getMinZ()),new BlockVector(permit.getMaxX(),permit.getMaxY(),permit.getMaxZ()));
		
		try {
			region.setParent(parent);
		} catch (CircularInheritanceException e) {
			rm.removeRegion(args.getString(0)+"_grid");
			rm.removeRegion(args.getString(0)+"_permit");
			sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.create.failed"));
			return;
		}
		rm.addRegion(region);
		
		
		sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.create.created"));
		
	}
	
	@Command(aliases = { "delete", "d", "remove", "r" },usage = "", flags = "", desc = "Delete an existing permit.",help = "<Permit Name>", min = 1, max = 1)
	@CommandPermissions("dwdplots.permit.delete")
	public static void delete(CommandContext args, CommandSender sender) throws CommandException {
		// Loop through states to find permit
			Collection<State> states = States.getStates();
			
			for(State state : states) {
				if(state.getPermit(args.getString(0)) != null) {
					Permit permit = state.getPermit(args.getString(0));
					
						state.getRegionManager().removeRegion(permit.getRegion().getId());
						
						permit.getState().deletePermit(permit);
						
						sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.deleted"));
					return;
				}
			}
			sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.permitNotFound"));
	}
	
	@Command(aliases = { "info", "i" },usage = "", flags = "", desc = "Get information on a permit.",help = "<Permit Name>", min = 1, max = 1)
	@CommandPermissions("dwdplots.permit.info")
	public static void info(CommandContext args, CommandSender sender) throws CommandException {
		
		// Loop through states to find permit
		Collection<State> states = States.getStates();
		
		for(State state : states) {
			if(state.getPermit(args.getString(0)) != null) {
				Permit permit = state.getPermit(args.getString(0));
				permit.printInfo((Player) sender);
				return;
			}
		}
		
		sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.permitNotFound"));
		
	}
	
	@Command(aliases = { "setowner", "owner" },usage = "", flags = "", desc = "Set the owner of a permit.",help = "<Permit Name> <Player>", min = 2, max = 2)
	@CommandPermissions("dwdplots.permit.setowner")
	public static void setowner(CommandContext args, CommandSender sender) throws CommandException {
		// Loop through states to find permit
		Collection<State> states = States.getStates();
		
		for(State state : states) {
			if(state.getPermit(args.getString(0)) != null) {
				Permit permit = state.getPermit(args.getString(0));
					permit.setSuperOwner(args.getString(1));
					
					ProtectedRegion region = permit.getRegion();
					
					DefaultDomain members = new DefaultDomain();
					members.addPlayer(args.getString(1));
					
					for(PermitMember member : permit.getMembers()) {
						if(member.hasFlag(PermitFlag.PERMIT_BUILD)) {
							members.addPlayer(member.getName());
						}
					}
					
					region.setMembers(members);
					
					sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.setowner").replaceAll("%P", args.getString(1).replaceAll("%N", permit.getName())));
				return;
			}
		}
		sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.permitNotFound"));
	}
	
	@Command(aliases = { "expand", "ex"},usage="", flags = "", desc = "Expand your permit.", help = "<Size>", min = 1, max = 1)
	@CommandPermissions("dwdplots.permit.expand")
	public static void expand(CommandContext args, CommandSender sender) throws CommandException {
		// Get users permit
		Player player = (Player) sender;
		Permit permit = States.getPermitFromPlayer(player);
		if(permit != null) {
			if(permit.memberHas(player.getName(), PermitFlag.PERMIT_EXPAND)) {
				
			}
			else {
				sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.missingFlag").replaceAll("%F","PERMIT_EXPAND"));
			}
		}
		else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.doesntOwnPermit"));
		}
	}
	
	@Command(aliases = { "setradius", "sr"},usage="", flags = "", desc = "Set the radius of a permit", help = "<Permit> <Radius>", min = 2, max = 2)
	@CommandPermissions("dwdplots.permit.setradius")
	public static void setradius(CommandContext args, CommandSender sender) throws CommandException {
		if(States.getPermitFromName(args.getString(0)) != null) {
			Permit permit = States.getPermitFromName(args.getString(0));
			permit.setRegionRadius(args.getInteger(1));
			sender.sendMessage(DwDPlotsPlugin.getLang().get("commands.permit.setradius").replaceAll("%N", permit.getName()).replaceAll("%S",(permit.getRadius()*2)+"x"+(permit.getRadius()*2)));
		}
		else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.permitNotFound"));
		}
	}
	
	@Command(aliases = { "cm" },usage="", flags = "", desc = "Council Member Commands", help = "", min = 0, max = 0)
	@NestedCommand(value = { PermitCMCommands.class })
	public static void cm(CommandContext args, CommandSender sender) throws CommandException {
	}
	
	@Command(aliases = { "flag", "f" },usage = "", flags = "", desc = "Set a flag for the permit",help = "<Flag> [Value] (Blank to remove)", min = 1, max = -1)
	@CommandPermissions("dwdplots.permit.flag")
	public static void flag(CommandContext args, CommandSender sender) throws CommandException {
		// Get users permit
		Player player = (Player) sender;
		Permit permit = States.getPermitFromPlayer(player);
		if(permit != null) {
			if(permit.memberHas(player.getName(), PermitFlag.FLAG_SET)) {
				
			}
			else {
				sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.missingFlag").replaceAll("%F","FLAG_SET"));
			}
		}
		else {
			sender.sendMessage(DwDPlotsPlugin.getLang().get("exceptions.doesntOwnPermit"));
		}
	}
	
	@Command(aliases = { "flags", "fs" },usage = "", flags = "", desc = "List available flags",help = "[page]", min = 0, max = 1)
	@CommandPermissions("dwdplots.permit.flags")
	public static void flags(CommandContext args, CommandSender sender) throws CommandException {
		
		
	}
}
